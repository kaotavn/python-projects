from bokeh.plotting import figure, output_file, show, ColumnDataSource
from bokeh.models.tools import HoverTool
import pandas as pd

#Read in stats.csv
data = pd.read_csv('stats.csv')

#Creating a ColumnDataSource from data
source= ColumnDataSource(data)

output_file('League_Stats.html') #  outputs glyph as an html file

#Legend List
lgnd_list = source.data['Legend'].tolist()

#Mastery Level List
ms_list= source.data['MasteryLevel'].tolist()

#plotting
p = figure(y_range=lgnd_list,
           plot_width=800,
           plot_height=4000,
           title='League Stats',
           x_axis_label='Mastery Level',
           tools='pan, box_select, zoom_in, zoom_out, reset, save' #tools user can use when viewing glyph in browser
)

#Render Glyph
p.hbar(
       y='Legend',
       right='MasteryLevel',
       left=0,
       height=0.3,
       color= 'green',
       fill_alpha=0.6,
       source=source
)

#Add Tooltips; when mouse is over bar(s) info is displayed; h3=header per popup box, rest=supplementary info
hover = HoverTool()
hover.tooltips = '''
    <div>
        <h3>@Legend</h3>
        <div><strong>Class: </strong>@Class</div>
        <div><strong>Mastery Level: </strong>@MasteryLevel</div>
        <div><img src='@Image' alt='' width='125' /</div>
    </div>

'''

p.add_tools(hover)


show(p)

'''
#Print out div and script; can be used for any other file
script, div = components(p)
print(div)
print(script)
'''
